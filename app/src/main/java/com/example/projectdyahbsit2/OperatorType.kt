package com.example.projectdyahbsit2

object OperatorType {
    //tambah
    //naming conversion java, untuk object atau var statis menggunakan huruf kapital
    const val TAMBAH = 1
    const val KURANG = 2
    const val KALI = 3
    const val BAGI = 4
}